/**
 Input: Cho 3 số nguyên



 Step: lấy từng số chia cho 2. Nếu chia hết thì tăng biến count
       => Số lẻ = 3 - count;



Output: Có bao nhiêu số chẵn, lẻ
 */

function demSoChan() {
  const a = document.getElementById("num1").value * 1;
  const b = document.getElementById("num2").value * 1;
  const c = document.getElementById("num3").value * 1;
  var count = 0;

  if (a % 2 == 0) {
    count++;
  }
  if (b % 2 == 0) {
    count++;
  }
  if (c % 2 == 0) {
    count++;
  }
  document.getElementById(`result`).value = `Có ${count} số chẵn, ${
    3 - count
  } số lẻ`;
}
