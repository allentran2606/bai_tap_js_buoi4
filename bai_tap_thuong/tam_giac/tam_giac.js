/*
Input: 3 cạnh của tam giác


Step: 
    + Xét 3 cạnh đó có tạo thành tam giác hay không (bất đẳng thức tam giác)
    + Cạnh a = b và a = c => Tam giác đều
    + Cạnh a = b hoặc a = c => Tam giác cân
    + Trường hợp cuối là tam giác vuông
*/

function tamGiac() {
  const a = document.getElementById("num1").value * 1;
  const b = document.getElementById("num2").value * 1;
  const c = document.getElementById("num3").value * 1;

  if (a + b <= c || a + c <= b || b + c <= a) {
    document.getElementById(`result`).value = `Không phải tam giác`;
  } else if (a == b && b == c) {
    document.getElementById(`result`).value = `Tam giác đều`;
  } else if (a == b || b == c) {
    document.getElementById(`result`).value = `Tam giác cân`;
  } else {
    document.getElementById(`result`).value = `Tam giác vuông`;
  }
}
