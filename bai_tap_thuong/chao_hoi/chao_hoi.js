/*
Input: 1 trong 4 thành viên


Step: Dùng switch case. Rơi vào trường hợp nào thì in ra câu chào tương ứng


Output: Câu chào hỏi
*/

function guiLoiChao() {
  const temp = document.getElementById("select").value;

  switch (temp) {
    case "B": {
      document.getElementById(`result`).value = `Xin chào Bố!`;
      break;
    }
    case "M": {
      document.getElementById(`result`).value = `Xin chào Mẹ!`;
      break;
    }
    case "A": {
      document.getElementById(`result`).value = `Xin chào Anh Trai!`;
      break;
    }
    case "E": {
      document.getElementById(`result`).value = `Xin chào Em Gái!`;
      break;
    }
    default: {
      document.getElementById(`result`).value = `default`;
    }
  }
}
