/*
 Input: 3 số nguyên


 Step: TH1: Nếu a > b và a > c => so sánh b và c => a lớn nhất, sau đó là b và c
       TH2: Nếu b > a và b > c => so sánh a và c => b lớn nhất, sau đó là a và c
       TH3: Nếu c > a và c > b => so sánh a và b => c lớn nhất, sau đó là a và b

 Output: 3 số a, b, c theo thứ tự tăng dần
 */

function sapXep() {
  const a = document.getElementById("num1").value * 1;
  const b = document.getElementById("num2").value * 1;
  const c = document.getElementById("num3").value * 1;

  if (a > b && a > c) {
    if (b > c) {
      document.getElementById(`result`).value = `${c}, ${b}, ${a}`;
    } else {
      document.getElementById(`result`).value = `${b}, ${c}, ${a}`;
    }
  } else if (b > a && b > c) {
    if (a > c) {
      document.getElementById(`result`).value = `${c}, ${a}, ${b}`;
    } else {
      document.getElementById(`result`).value = `${a}, ${c}, ${b}`;
    }
  } else if (c > a && c > b) {
    if (a > b) {
      document.getElementById(`result`).value = `${b}, ${a}, ${c}`;
    } else {
      document.getElementById(`result`).value = `${a}, ${b}, ${c}`;
    }
  }
}
