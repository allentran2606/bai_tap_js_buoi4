function tinhQuangDuongXaNhat() {
  const ten_SV1 = document.getElementById("name1").value;
  const ten_SV2 = document.getElementById("name2").value;
  const ten_SV3 = document.getElementById("name3").value;
  var sv1_x = document.getElementById("sv1-x").value * 1;
  var sv1_y = document.getElementById("sv1-y").value * 1;
  var sv2_x = document.getElementById("sv2-x").value * 1;
  var sv2_y = document.getElementById("sv2-y").value * 1;
  var sv3_x = document.getElementById("sv3-x").value * 1;
  var sv3_y = document.getElementById("sv3-y").value * 1;
  var school_x = document.getElementById("school-x").value * 1;
  var school_y = document.getElementById("school-y").value * 1;
  // Tính quãng đường
  d1 = Math.sqrt(
    (school_x - sv1_x) * (school_x - sv1_x) +
      (school_y - sv1_y) * (school_y - sv1_y)
  );
  d2 = Math.sqrt(
    (school_x - sv2_x) * (school_x - sv2_x) +
      (school_y - sv2_y) * (school_y - sv2_y)
  );
  d3 = Math.sqrt(
    (school_x - sv3_x) * (school_x - sv3_x) +
      (school_y - sv3_y) * (school_y - sv3_y)
  );
  // Tìm max
  var maxValue = null;
  maxValue = d1;
  if (d2 > maxValue) {
    maxValue = d2;
    document.getElementById(
      `result`
    ).innerHTML = `Sinh viên xa trường nhất: ${ten_SV2}`;
  }
  if (d3 > maxValue) {
    maxValue = d3;
    document.getElementById(
      `result`
    ).innerHTML = `Sinh viên xa trường nhất: ${ten_SV3}`;
  }

  document.getElementById(
    `result`
  ).innerHTML = `Sinh viên xa trường nhất: ${ten_SV1}`;
}
