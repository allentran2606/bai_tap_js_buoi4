var thangHai = null;

function tinhNgayCu() {
  const ngay = document.getElementById("date").value * 1;
  const thang = document.getElementById("month").value * 1;
  const nam = document.getElementById("year").value * 1;
  var temp = 0;

  // Kiểm tra năm nhuận
  if (nam % 4 == 0 && nam % 100 != 0) {
    thangHai = 29;
  } else {
    thangHai = 28;
  }

  // Kiểm tra ngày 1/1 năm mới
  if (ngay == 1 && thang == 1) {
    document.getElementById(`result`).value = `${31}/${12}/${nam - 1}`;
    temp++;
    //Biến temp dùng để xác định xem trường hợp ngày 1/1 của năm mới đã thực hiện hay chưa
  }

  if (ngay == 1 && (thang == 5 || thang == 7 || thang == 10 || thang == 12)) {
    yesterday = 30;
    lastMonth = thang - 1;
    document.getElementById(
      `result`
    ).value = `${yesterday}/${lastMonth}/${nam}`;
  } else if (
    ngay == 1 &&
    (thang == 2 ||
      thang == 4 ||
      thang == 6 ||
      thang == 8 ||
      thang == 9 ||
      thang == 11)
  ) {
    yesterday = 31;
    lastMonth = thang - 1;
    document.getElementById(
      `result`
    ).value = `${yesterday}/${lastMonth}/${nam}`;
  } else if (ngay == 1 && thangHai == 29 && thang == 3) {
    yesterday = 29;
    lastMonth = thang - 1;
    document.getElementById(
      `result`
    ).value = `${yesterday}/${lastMonth}/${nam}`;
  } else if (ngay == 1 && thang == 3) {
    yesterday = 28;
    lastMonth = thang - 1;
    document.getElementById(
      `result`
    ).value = `${yesterday}/${lastMonth}/${nam}`;
  } else if (temp != 1) {
    //Biến temp khác 1 nghĩa là trường hợp 1/1 chưa thực hiện rồi, tránh trừ lại nhiều lần
    yesterday = ngay - 1;
    document.getElementById(`result`).value = `${yesterday}/${thang}/${nam}`;
  }
}

function tinhNgayMoi() {
  const ngay = document.getElementById("date").value * 1;
  const thang = document.getElementById("month").value * 1;
  const nam = document.getElementById("year").value * 1;
  var temp = 0;

  if (nam % 4 == 0 && nam % 100 != 0) {
    thangHai = 29;
  } else {
    thangHai = 28;
  }

  if (ngay == 31 && thang == 12) {
    document.getElementById(`result-2`).value = `${1}/${1}/${nam + 1}`;
    temp++;
  }
  if (
    ngay == 31 &&
    (thang == 1 ||
      thang == 3 ||
      thang == 5 ||
      thang == 7 ||
      thang == 8 ||
      thang == 10)
  ) {
    tomorrow = 1;
    nextMonth = thang + 1;
    document.getElementById(`result`).value = `${tomorrow}/${nextMonth}/${nam}`;
  } else if (
    ngay == 30 &&
    (thang == 4 || thang == 6 || thang == 9 || thang == 11)
  ) {
    tomorrow = 1;
    nextMonth = thang + 1;
    document.getElementById(`result`).value = `${tomorrow}/${nextMonth}/${nam}`;
  } else if (thangHai == 29 && thang == 2 && ngay == 29) {
    tomorrow = 1;
    nextMonth = thang + 1;
    document.getElementById(`result`).value = `${tomorrow}/${nextMonth}/${nam}`;
  } else if (ngay == 28 && thang == 2 && thangHai == 28) {
    tomorrow = 1;
    nextMonth = thang + 1;
    document.getElementById(`result`).value = `${tomorrow}/${nextMonth}/${nam}`;
  } else if (temp != 1) {
    tomorrow = ngay + 1;
    document.getElementById(`result`).value = `${tomorrow}/${thang}/${nam}`;
  }
}
