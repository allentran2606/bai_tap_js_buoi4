function docSo() {
  const number = document.getElementById("num").value * 1;

  donVi = number % 10;
  hangChuc = Math.floor(number / 10) % 10;
  tram = Math.floor(number / 100);

  switch (tram) {
    case 1: {
      hundred = `Một trăm`;
      break;
    }
    case 2: {
      hundred = `Hai trăm`;
      break;
    }
    case 3: {
      hundred = `Ba trăm`;
      break;
    }
    case 4: {
      hundred = `Bốn trăm`;
      break;
    }
    case 5: {
      hundred = `Năm trăm`;
      break;
    }
    case 6: {
      hundred = `Sáu trăm`;
      break;
    }
    case 7: {
      hundred = `Bảy trăm`;
      break;
    }
    case 8: {
      hundred = `Tám trăm`;
      break;
    }
    case 9: {
      hundred = `Chín trăm`;
      break;
    }
  }

  switch (hangChuc) {
    case 1: {
      tyEl = `mười`;
      break;
    }
    case 2: {
      tyEl = `hai mươi`;
      break;
    }
    case 3: {
      tyEl = `ba mươi`;
      break;
    }
    case 4: {
      tyEl = `bốn mươi`;
      break;
    }
    case 5: {
      tyEl = `năm mươi`;
      break;
    }
    case 6: {
      tyEl = `sáu mươi`;
      break;
    }
    case 7: {
      tyEl = `bảy mươi`;
      break;
    }
    case 8: {
      tyEl = `tám mươi`;
      break;
    }
    case 9: {
      tyEl = `chín mươi`;
      break;
    }
  }

  switch (donVi) {
    case 1: {
      unit = `một`;
      break;
    }
    case 2: {
      unit = `hai`;
      break;
    }
    case 3: {
      unit = `ba`;
      break;
    }
    case 4: {
      unit = `bốn`;
      break;
    }
    case 5: {
      unit = `năm`;
      break;
    }
    case 6: {
      unit = `sáu`;
      break;
    }
    case 7: {
      unit = `bảy`;
      break;
    }
    case 8: {
      unit = `tám`;
      break;
    }
    case 9: {
      unit = `chín`;
      break;
    }
  }
  document.getElementById(`result`).value = `${hundred} ${tyEl} ${unit}`;
}
