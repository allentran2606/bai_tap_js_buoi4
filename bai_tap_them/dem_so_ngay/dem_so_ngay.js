function tinhNgay() {
  const thang = document.getElementById("month").value * 1;
  const nam = document.getElementById("year").value * 1;
  var temp = null;
  // Xét năm nhuận
  if (nam % 4 == 0 && nam % 100 != 0) {
    thangHai = 29;
  } else {
    thangHai = 28;
  }

  if (thangHai == 29 && thang == 2) {
    soNgay = thangHai;
    document.getElementById(
      `result`
    ).value = `Tháng ${thang} năm ${nam} có ${soNgay} ngày`;
    temp++;
  } else if (thangHai == 28 && thang == 2) {
    soNgay = 28;
    document.getElementById(
      `result`
    ).value = `Tháng ${thang} năm ${nam} có ${soNgay} ngày`;
    temp++;
  }
  if (
    thang == 1 ||
    thang == 3 ||
    thang == 5 ||
    thang == 7 ||
    thang == 8 ||
    thang == 10 ||
    thang == 12
  ) {
    soNgay = 31;
    document.getElementById(
      `result`
    ).value = `Tháng ${thang} năm ${nam} có ${soNgay} ngày`;
  } else if (temp != 1) {
    soNgay = 30;
    document.getElementById(
      `result`
    ).value = `Tháng ${thang} năm ${nam} có ${soNgay} ngày`;
  }
}
